//membuatkaliamat
var word = 'JavaScript';
var second = 'is';
var third = 'awesome';
var fourth = 'and';
var fifth = 'I';
var sixth = 'love';
var seventh = 'it!';

var res = word + " " + second + " " + third + " " + fourth + " " + fifth +  " " + sixth + " " + seventh + " " ;

console.log(res);

//menguraikalimat
var sentence = "I am going to be React Native Developer";
var FirstWord = "i" ;
var secondWord = "am" ;
var thirdWord = "going" ; 
var fourthWord = "to";
var fifthWord = "be";
var sixthWord = "react"; 
var seventhWord = "native"; 
var eighthWord = "developer"; 
console.log('First Word: ' + FirstWord);
console.log('Second Word: ' + secondWord);
console.log('Third Word: ' + thirdWord);
console.log('Fourth Word: ' + fourthWord);
console.log('Fifth Word: ' + fifthWord);
console.log('Sixth Word: ' + sixthWord);
console.log('Seventh Word: ' + seventhWord);
console.log('Eighth Word: ' + eighthWord);

//substring
var sentence2 = 'wow JavaScript is so cool';

var FirstWord2 = "wow";
var secondWord2 = "JavaScript"; 
var thirdWord2 = "is";
var fourthWord2 = "so";
var fifthWord2 = "cool";
console.log('First Word: ' + FirstWord2);
console.log('Second Word: ' + secondWord2);
console.log('Third Word: ' + thirdWord2);
console.log('Fourth Word: ' + fourthWord2);
console.log('Fifth Word: ' + fifthWord2);

//mengurai kalimat dan menentukan panjang string
var sentence3 = 'wow JavaScript is so cool';
var FirstWord3 = sentence3.substring(0, 3);
var secondWord3 = sentence3.substring(4, 14); 
var thirdWord3 = sentence3.substring(15, 17); 
var fourthWord3 = sentence3.substring(18, 20); 
var fifthWord3 = sentence3.substring(21, 25); 
var firstWordLength = FirstWord3.length
var secondWordLength = secondWord3.length
var thirdWordLength = thirdWord3.length
var fourthWordLength = fourthWord3.length
var fifthWordLength = fifthWord3.length
console.log('First Word: ' + FirstWord3 + ', with length: ' + firstWordLength);
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWordLength);
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWordLength);
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWordLength);
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWordLength);
